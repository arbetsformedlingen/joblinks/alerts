#!/bin/bash
#set -eEu -o pipefail
self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

report=$(mktemp)
status=0


# if nr of imported ads are lower than this - warn. FIXME: make a parameter instead of hardcode
LOWERLIMIT=50000

# opensearch/elastic alias to check. FIXME: make a parameter instead of hardcode
ALIAS="joblinks-pipeline-stdin"


function alert() {
    local status="$1"
    local report="$2"
    { echo '```'
      echo
      echo -e "JobTechLinks result monitor detected $status error(s).\n\n"
      echo
      cat $report
      echo '```'
    } | bash -x /app/msgsend.sh /secrets/mattermost_conf.sh /secrets/mattermost_mattersendalert.conf
}

set -x

bash -x src/test0.sh "$ALIAS" "$LOWERLIMIT" >>$report
if [ $? != 0 ]; then
    status=$(( status + 1 ))
fi

bash -x src/test1.sh "$ALIAS" >>$report
if [ $? != 0 ]; then
    status=$(( status + 1 ))
fi

bash -x src/test2.sh >>$report
if [ $? != 0 ]; then
    status=$(( status + 1 ))
fi

if [ "$status" != 0 ]; then
    alert "$status" "$report"
fi

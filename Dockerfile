FROM registry.gitlab.com/arbetsformedlingen/joblinks/chat-notify/master:32589e787360f17e6dc7e06c937ade96fbd7d6ed

ADD run.sh /app/
ADD src /app/src/

RUN apt-get -y update &&\
    apt-get -y --no-install-recommends install sqlite3 &&\
    chmod a+rx /app/*.sh /app/src/*.sh

WORKDIR /app

CMD [ "/app/run.sh" ]

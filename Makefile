IMG = pipeline-alerts

ENV = test
DOCKERFLAGS = -v $(PWD)/secrets/$(ENV):/secrets:Z

run:
	docker run $(DOCKERFLAGS) --rm -i $(IMG) /app/run.sh $(ENV)

build:
	docker build -t $(IMG) .

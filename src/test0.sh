#!/bin/bash
set -eEu -o pipefail

self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

alias="$1"
LOWERLIMIT="$2"

trap "echo '**** failed to find an index for' $alias" ERR
index=$(bash "$self_dir"/list_indices.sh | grep -E "$alias-[0-9]{8}" | head -n 1 | awk '{print $2}')
trap - ERR

trap "echo '**** failed to find ad count for' $alias" ERR
imported=$(bash "$self_dir"/get_number_of_ads_in_index.sh "$index")
[[ $imported =~ ^-?[0-9]+$ ]] || false
trap - ERR

echo "checking index $index, that $imported >= $LOWERLIMIT" >&2

if [ "$imported" -lt "$LOWERLIMIT" ]; then
    echo "**** suspiciously low number of imported ads in index $index: $imported"
    exit 1
fi

#!/bin/bash
set -eEu -o pipefail

. /secrets/env_cloudelastic.sh

curl --silent -X GET -u "$ES_USER_READ:$ES_PWD_READ" "$ES_HOST/_cat/aliases?v"

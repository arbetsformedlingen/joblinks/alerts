#!/bin/bash
set -eEu -o pipefail


rm -rf "/tmp/activity-data-collector-data-main-*"


# Download time series and db.sqlite for pipeline-stats
cd /tmp
trap "echo '**** failed to download scraper statistics'" ERR
curl -L -O 'https://gitlab.com/api/v4/projects/30191577/repository/archive.tar.gz'
tar -zxf archive.tar.gz
cd activity-data-collector-data-main-*/pipeline-stats
trap - ERR


### Determine list of partners
trap "echo '**** failed to download prod configuration to determine version hash'" ERR
version=$(curl https://gitlab.com/arbetsformedlingen/joblinks/pipeline-gitops-for-servers/-/raw/master/bin/run_prod.sh | grep ^PIPELINECOMMIT | sed -e 's|PIPELINECOMMIT=||' -e 's|[[:space:]]*||g')
trap - ERR
trap "echo '**** failed to download list of partners from prod configuration'" ERR
partners=$({ curl https://gitlab.com/arbetsformedlingen/joblinks/pipeline/-/raw/"$version"/conf/common_settings.sh ; echo 'echo $include_import,$exclude_import' ; } | bash - | tr ',' ' ')
trap - ERR



DATE="$(date +%Y-%m-%d)"

MIDNIGHT_EPOCH=$(date +%s -d $DATE"T00:00:00")

status=0
errfile=/tmp/report.$$

for P in $partners; do
    COUNT=0

    ID=$(sqlite3 db.sqlite "SELECT scraper_id FROM scraper WHERE name = '$P'")

    if [ -f "$ID".csv ]; then
        LINE=$(tail -n 1 "$ID".csv)
        EPOCH=$(echo "$LINE" | perl -pe 's|^([^,]+),[^,]+$|$1|')
        if [ "$EPOCH" -gt "$MIDNIGHT_EPOCH" ]; then
            COUNT=$(echo "$LINE" | perl -pe 's|^[^,]+,([^,]+)$|$1|')
        fi
    fi

    if [ "$COUNT" = 0 ]; then
        status=$(( status + 1 ))
        echo "**** scraper $P produced 0 ads" >> "$errfile"
    fi
done

echo "checking that all scrapers have produced ads" >&2

if [ "$status" != "0" ]; then
    cat $errfile >&2

    cat $errfile
    exit $status
fi

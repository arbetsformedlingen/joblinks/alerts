#!/bin/bash
set -eEu -o pipefail

set -x
index="$1"


. /secrets/env_cloudelastic.sh


VALUE=$(curl --silent -X POST -u "$ES_USER_READ:$ES_PWD_READ" -H 'Content-Type: application/json' "$ES_HOST/$index/_search?track_total_hits=true" -d'{
  "sort": {
    "date_to_display_as_publish_date": "desc"
  }
}' | jq '.hits.total.value')

[[ $VALUE =~ ^-?[0-9]+$ ]] || exit 1

echo $VALUE

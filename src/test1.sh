#!/bin/bash
set -eEu -o pipefail

self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

alias="$1"

trap "echo '**** failed to find an index for' $alias" ERR
index=$(bash "$self_dir"/list_indices.sh | grep -E "$alias-[0-9]{8}" | head -n 1 | awk '{print $2}')
trap - ERR

trap "echo '**** failed to parse date of index' $index" ERR
indexdate=$(echo "$index" | perl -pe 's/^.*-(\d\d\d\d\d\d\d\d)-\d\d.\d\d$/$1/')
[[ $indexdate =~ ^-?[0-9]+$ ]] || false
trap - ERR

realdate=$(date +%Y%m%d)

echo "checking index $index that date $indexdate is $realdate" >&2

if [ "$indexdate" != "$realdate" ]; then
    echo "**** index date indicates that no import has been made: $indexdate"
    exit 1
fi
